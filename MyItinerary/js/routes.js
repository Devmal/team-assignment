//This file displays a map of the country the user selected, showing routes available from the source arport selected
//the user is then able to select routes from each airport in order to plan their trip
//The file accesses this information (all available routes, all airports) from the OpenFlights api and displays the map using the MapBox api
//Team: Shathurshan, Devmal, Zaeema
//Last modified: 16/05/2021
//"use strict";
getDataLocalStorage()

let sourceCountry = trips.trips[trips.trips.length-1]._country
var sourceAirport = trips.trips[trips.trips.length-1]._source
trips.trips[trips.trips.length-1].addRoute(sourceAirport)

//trips.trips[trips.trips.length-1].routes[trips.trips[trips.trips.length-1].routes.length-1].originCoord

//let times = 0;
var sourceAirportCode = ""
var sourceAirportlat = ""
var sourceAirportlong = ""
var currentMarkers = []
var sourceAirports = [];
var sourceAirportCoordinates = [];
var finalCoordinates = [];
    
// Array to store airports in a country
let airports = [];

let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v10',
    zoom: 5
    });
   
function showRoutes()
{
    let airportsData = {
    country: String(sourceCountry),
    callback: "airportsResponse"
    };
    webServiceRequest("https://eng1003.monash/OpenFlights/airports/", airportsData); //requesting airport data
}

showRoutes()

//webServiceRequest("https://eng1003.monash/OpenFlights/airports/", airportsData); //requesting airport data

//This function accepts airport data (from the OpenFlights api)
//and uses the data to generate a map and allow users to select routes
//argument: airports: this is data from the OpenFlights api containing a list of airports in the country
//as well as relevant information for each airport (airport ID, airport name, IATA-FAA code, city, country, latitude, longitude,altitude)
this.airportsResponse = function(airports)
    {
        console.log(airports)
        
        //looking for relelvant information for source airport
        for(var i = 0;i<airports.length;i++)
        {
            if(sourceAirport === airports[i].name)
            {
                sourceAirportCode = airports[i].airportId;
                sourceAirportlat = airports[i].latitude;
                sourceAirportlong = airports[i].longitude;
            }
        }
        trips.trips[trips.trips.length-1].routes[trips.trips[trips.trips.length-1].routes.length-1].originCoord=[Number(sourceAirportlong),Number(sourceAirportlat)]
        //trips.trips[trips.trips.length-1].routes[trips.trips[trips.trips.length-1].routes.length-1].destinationCoord
        finalCoordinates.push([Number(sourceAirportlong),Number(sourceAirportlat)])

        map.flyTo({center:[Number(sourceAirportlong),Number(sourceAirportlat)]})
            
        //  Array to store route data from the source airport
        let routes = [];
             
        //  Array to store route data within a country from the source airport
        let routesWithinCountry = [];
        // Object with stored data to request routes of source airport using the API
        // "https://eng1003.monash/OpenFlights/routes/"
            
        let routesData = {
            sourceAirport:String(sourceAirportCode),
            callback: "routesResponse"
        };
            
        webServiceRequest("https://eng1003.monash/OpenFlights/routes/", routesData)

        //This is a function that receives the routes from the source airport
        //and uses the map to allow users to select their required routes
        //argument: routes: this is data from OpenFlights containing a list of routes from a given airport and other relevant info for each route
        //(airline, airline ID, source airport, source airport ID, destination airport, destination airport ID, codeshare, stops, equipment)
        this.routesResponse = function(routes)
            {
                //console.log(routes)
                
                var stopDetailsArray = [];    
               
                // For loop to filter the routes of the source airport to only access routes within the country
                for (var i =0;i<routes.length;i++)
                    {
                        var ID = String(routes[i].destinationAirportId);
                          
                        for(var j =0 ;j < airports.length;j++)
                        { 
                            if (ID === airports[j].airportId)
                            {
                                routesWithinCountry.push(airports[j])
                                stopDetailsArray.push(routes[i].stops)
                            }                                   
                        }
                    }
                 
                //console.log(routesWithinCountry)
                
                // This variable stores the coordinates of the source Airport  
                var origin = [Number(sourceAirportlong),Number(sourceAirportlat)];

                // Indicating the source airport to the user, by creating a marker on the airport
                var el = document.createElement('div');
                el.className = 'marker';
                el.style.backgroundImage = 'url(https://cdn.rawgit.com/mapbox/mapbox-gl-styles/master/sprites/bright-v9/_svg/airfield-15.svg)';
                el.style.width = '20px';
                el.style.height = '20px';
                el.style.backgroundColor = 'red'
   
                // Displaying the marker based on the source airport Location 
                {
                    var marker = new mapboxgl.Marker(el)
                        .setLngLat({lng:Number(sourceAirportlong),lat:Number(sourceAirportlat)})
                        .addTo(map)

                        currentMarkers.push(marker)
                };

                //var IconData = [];

                if (sourceAirports.length > 0)// && sourceAirportCoordinates.length > 0 && sourceAirportCoordinates.length === sourceAirports.length)
                {
                    for(var i = 0;i < sourceAirports.length;i++)
                    {
                        var el = document.createElement('div');
                        el.className = 'marker';
                        el.style.backgroundImage = 'url(https://cdn.rawgit.com/mapbox/mapbox-gl-styles/master/sprites/bright-v9/_svg/airfield-15.svg)';
                        el.style.width = '20px';
                        el.style.height = '20px';
                        el.style.backgroundColor = '#00ffff'

                  // Displaying the marker based on the source airport Location
                        {
                            var marker = new mapboxgl.Marker(el)
                                .setLngLat(sourceAirportCoordinates[i])
                                .addTo(map)

                                currentMarkers.push(marker)

                        };

                    }
                }
                
                var IconData = [];

                // Array to store objects. These objects will be used as data to draw polylines as routes.
                var polylineData = [];

                var Intiallinecolor = '#00C000'

                for(let i = 0;i < routesWithinCountry.length;i++)
                {
                    //creating icons for each airport marker (IS THIS CORRECT?)
                   var data = {
                       'type': 'Feature',
                       'properties': {
                       'name': routesWithinCountry[i].name,   
                       'icon-color': '#00ffff'
                                },
                       'geometry': {
                       'type': 'Point',
                       'coordinates': [Number(routesWithinCountry[i].longitude),Number(routesWithinCountry[i].latitude)],
                       'stops': stopDetailsArray[i]
                        }
                    }
                    
                    IconData.push(data)
                    
                    if (i !== routesWithinCountry.length -1)
                    {
                        IconData.push(',')    
                    }
                }

                var geojson = {
                    'type': 'FeatureCollection',
                    'features':[IconData]
                }
                                      
                geojson.features[0].forEach(function (marker) {      
                    if (marker !== ",")
                    {
                        // Create a DOM element for each marker.
                        var el = document.createElement('div');
                        el.className = 'marker';
                        el.style.backgroundImage = 'url(https://cdn.rawgit.com/mapbox/mapbox-gl-styles/master/sprites/bright-v9/_svg/airfield-15.svg)';
                        el.style.width = '20px';
                        el.style.height = '20px';
                        el.style.backgroundColor = '#00ffff'
                        el.style.cursor = "pointer"

                        el.addEventListener('click', function () {
                            var result = confirm('You have chosen ' + marker.properties.name + ' as your destination Airport \nDo you wish to continue? ');
                            if (result === true)
                            {
                                //console.log(marker.properties.name)
                                //console.log(marker.geometry.stops)
                                
                                finalCoordinates.push(marker.geometry.coordinates)
                                sourceAirports.push(sourceAirport)
                                sourceAirportCoordinates.push([Number(sourceAirportlong),Number(sourceAirportlat)]) 

                                var options = {units: 'kilometres'}
                                var length = turf.distance(origin,marker.geometry.coordinates, options)
                                 
                                //console.log(length.toFixed(3)+"km")

                                trips.trips[trips.trips.length-1].routes[trips.trips[trips.trips.length-1].routes.length-1].destination=marker.properties.name; //making new airport clicked, the destination of previous class instance
                                trips.trips[trips.trips.length-1].routes[trips.trips[trips.trips.length-1].routes.length-1].destinationCoord=marker.geometry.coordinates; //setting the coordinates of new airport clicked as the destination coordinates of previous route instance
                                trips.trips[trips.trips.length-1].routes[trips.trips[trips.trips.length-1].routes.length-1].distance=length;
                                
                                 
                                trips.trips[trips.trips.length-1].addRoute(marker.properties.name); //adding a new route instance with the airport just clicked as the origin
                                trips.trips[trips.trips.length-1].routes[trips.trips[trips.trips.length-1].routes.length-1].originCoord=marker.geometry.coordinates; // setting the coordinates of new airport clicked as the origin coordinates of latest route made
                                map.removeLayer('multiple-lines-layer')

                                 map.removeSource('multiple-lines-source')


                                 sourceAirport = marker.properties.name;


                                if (currentMarkers!==null)
                                {
                                    for (var i = currentMarkers.length - 1; i >= 0; i--)
                                    {
                                        currentMarkers[i].remove();
                                    }
                                }

                                showRoutes()

                            }
                            
                            else
                            {console.log('cancelled')}
                        })

                        // Add markers to the map.
                        var oneMarker =  new mapboxgl.Marker(el)
                        .setLngLat(marker.geometry.coordinates)
                        .addTo(map);
                        currentMarkers.push(oneMarker)
                    }
                });

               
                // Function to display the routes
                function displayRoutes()
                {
                    for (var i = 0;i<routesWithinCountry.length;i++)
                    {
                        var data = 
                        {
                          'type': 'Feature',
                          'properties': {'name':routesWithinCountry[i].name,'color':Intiallinecolor},
                          'geometry': {
                          'type': 'LineString',
                          'coordinates': [origin,[Number(routesWithinCountry[i].longitude),Number(routesWithinCountry[i].latitude)]]
                               } 
                        }

                        if (data.properties.name === sourceAirports[sourceAirports.length-1])
                        {
                           data.properties.color = '#000'
                        }

                        polylineData.push(data)
             
                        if (i !== routesWithinCountry.length -1)
                       {
                         polylineData.push(',')    
                       }
                    }

                    if(sourceAirports.length > 1)// && sourceAirportCoordinates.length > 0)
                    {
                         for(var i = 0 ; i < sourceAirports.length-1;i++)
                        {
                          polylineData.push(',')

                          var data =
                         {
                          'type': 'Feature',
                          'properties': {'name':sourceAirports[i],'color':'#000'},
                          'geometry': {
                          'type': 'LineString',
                          'coordinates': [sourceAirportCoordinates[i],sourceAirportCoordinates[i+1]]
                               }
                         }

                          polylineData.push(data)

                        }
                    }

                    //console.log(sourceAirports)
                    //console.log(polylineData)

                    map.addSource('multiple-lines-source', {
                        'type': 'geojson',
                        'data': {
                        'type': 'FeatureCollection',
                        'features': polylineData
                              },
                        });
    
                    map.addLayer({
                        'id': 'multiple-lines-layer',
                        'type': 'line',
                        'source': 'multiple-lines-source',
                        'layout': {},
                        'paint': {
                            'line-color': ['get', 'color'],
                            'line-width': 2.5
                                },
                        });
                    };

                    displayRoutes()
                }

                
            }

            function Summary()
            {
                console.log(trips)
            
                updateLocalStorage(trips);
            
                localStorage.setItem("routeData",JSON.stringify(finalCoordinates));
            
                window.location.href = 'summary.html'
            
            }
            
function undo()
{
    //if(times < 6)
    //{
        map.removeLayer('multiple-lines-layer')
        map.removeSource('multiple-lines-source')
        console.log(sourceAirports);
        //sourceAirports.pop();
        if(sourceAirports.length !== 1)
        {
            sourceAirport = sourceAirports[sourceAirports.length-1];
            sourceAirports.pop()
            sourceAirportCoordinates.pop();

        }
        else
        {
            sourceAirport = trips.trips[trips.trips.length-1]._source;
            sourceAirports.pop();
            sourceAirportCoordinates.pop();

        }
        //trips.trips[trips.trips.length-1].removeRoute(finalCoordinates[finalCoordinates.length-1]);
        //trips.trips[trips.trips.length-1].removeRoute(finalCoordinates.length-1);
        trips.trips[trips.trips.length-1].removeRoute(trips.trips[trips.trips.length-1].routes.length-1);
        finalCoordinates.pop();
        finalCoordinates.pop();
        finalCoordinates.pop();
        //trips.trips(trips.trips[trips.trips.length-1]).removeOriginCoord()
        if (currentMarkers!==null) {
            for (var i = currentMarkers.length - 1; i >= 0; i--) {
                currentMarkers[i].remove();
                }
            }
        //times += 1;
        console.log(finalCoordinates);
        showRoutes();
    //}
    /*else
    {
    alert("You can't undo anymore!");
    }*/
}



                            
                 


/*Asks user to select a country from a drop down list. Asks user to select a Date
and time for the trip. Takes all the information given by the user and updates local storage.
Ones the user confirms the information the page takes the slected country so that only airports in that country will
in the list to choose in the next page*/

"use strict";

//populating "country" dropdown list with countries from countries.js
function selectCountry(){
    let select = document.getElementById("trip-country")
    for (let i=0;i<countryData.length;i++)
    {
        select.innerHTML+= '<option value="' + countryData[i] + '">'+ countryData[i] + '</option>'
    }
}

//function to store data from user input and then access relevant data from api
function planTrip()
{
    //Getting the name, country, date and time of the trip
    let tripName=document.getElementById("trip-name").value;
    let tripCountry=document.getElementById("trip-country").value;
    let tripDate=document.getElementById("trip-date").value;
    let tripTime=document.getElementById("trip-time").value;
    trips.addTrip(tripName,tripCountry,tripDate,tripTime);
    updateLocalStorage(trips);
    airportsAPI(); //calling function to get airport data based on country that user selects
}

//function that gets airport data from OpenFlight api
function airportsAPI()
{
    //Url to access all airports
    let url = "https://eng1003.monash/OpenFlights/airports/";
    //Getting particular country
    let country = trips.getTrip(trips.tripCount-1)._country;
    let data={
        country: country,
        callback:"selectSource"
    }
    //accessing all airports of the chosen country
    webServiceRequest(url,data);
}

//function that makes a dropdown list containing relevant airports for user to select
function selectSource(data)
{

    console.log(data)
    //Accessing html division
    let airportList=document.getElementById("schedule-content")
    //Creating output string
    let output = "";
    //Clearing html division
    airportList.innerHTML="";

    //adding html elements to output string
    output += '<div class="selectSource">';
    output += '<p class="select_text">Select the source airport</p><br>';
    output += '<div class="select_option select_source">';
    output += '<select id="source-airport" class="schedule_input" name="sourceAirport" class="form-control">';
    //looping through airports from api
    for (let i=0;i<data.length;i++)
    {
        output += '<option value="' + data[i].name + '">'+ data[i].name + '</option>';
    }
    output +='</select></div></div>';
    output+='<div class="plan button" style= "position:relative; left:200px; top: 20px">'
    output+= '<button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent mdl-button--colored" onclick="goToPlanTrip()">Plan Trip</button>'

    //Updating html division will drop down list
    airportList.innerHTML+=output;
}

//function that takes user to relevant map page
function goToPlanTrip()
{
    //Getting selected airport
    let tripSource=document.getElementById("source-airport").value;
    trips.getTrip(trips.tripCount-1).source=tripSource; //updating last trip created with airport selected by user
    updateLocalStorage(trips);
    window.location="routes.html" //redirecting to page with map to plan the trip
}

//Function to run when page loads
selectCountry()

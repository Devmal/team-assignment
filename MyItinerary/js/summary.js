/*Displays a summary of the scheduled trip to the user by accessing all necessary
information from local storage using methods and then gives the user the option confirming
or deleting the scheduled trip. If the user confirms the trip, then they are taken
to a detailed trip summary where a the planned route will be displayed on a map. If the
user delets the trip, then they will be taken back to the welcome page.*/
"use strict";
//funtion to diplay route summary

getDataLocalStorage()

function displaySummary()
{
  //Accessing html divion
  let outputArea = document.getElementById("summary");
  //Clearing default trip card
  outputArea.innerHTML="";
  //Setting output as an empty string to be updated with HTML elements
  let output = "";

  //Getting the lenght, name, date and distance of the trip
  //let length = trips.trips[trips.trips.length-1].length;
  ///let i = trips.trips.length-1; // index of the last scheduled trip
  let tripName = trips.trips[trips.trips.length-1].name;
  let tripDate = trips.trips[trips.trips.length-1].date;
  //let tripRoute = date.trips[i].route;

  //Creating a string of all the stops
  
  //let tripDistance = trips.trips[i].routes[i].distance;
  let tripTotDistance=0;
  let tripDistance=""
  for (let i=0;i<trips.trips[trips.trips.length-1].routes.length;i++)
  {
    tripTotDistance += Number(trips.trips[trips.trips.length-1].routes[i].distance)
    
  }
  tripDistance = tripTotDistance.toFixed(2) + " km"

  let tripPath = "";
  for (let i=0;i<trips.trips[trips.trips.length-1].routes.length;i++) //looping through all Route instances in the routes array
  {
    tripPath += trips.trips[trips.trips.length-1].routes[i].origin // where i represents the last trip made and j represents each route instance in the route array (which is inside that Trip)
    if(i!==trips.trips[trips.trips.length-1].routes.length-1)
    {
      tripPath += "-";
    }
  }

  /*let tripPath ="";

  for(let j=0; j<trips.trips[i].routes.length;j++)
  {
    tripPath += tripRoute[j];

    if(j!==tripRoute.length-1)
    {
      tripPath += "-";
    }
  }*/

  //Updating output will all the trip details
  output+="Trip Name:" +tripName+ "<br><br>" + "Date: "+tripDate+"<br><br>"+  "Route: "+tripPath+"<br><br>Total Distance: "+tripDistance;
  //Updating html division wth all the relevant information
    outputArea.innerHTML += output;

}

//function to confirm the scheduled trip
function confirmTrip()
{
  //updateLocalStorage(trips);

  //Directing user to the detailed trip information page
  window.location ="finalRoute.html"
}

//function to delete the scheduled trip
function cancelTrip()
{
  //Accesing the particular trip
  let length = trips.trips.length;
  let i = length-1;

  //Asking the user if they want to go ahead with deleting the trip
  if (confirm("Are you sure you want to delete this trip"))
  {
    //Deleting trip using the method
    trips.removeTrip(i);
    //Informing the user
    alert("Your trip has been deleted");
    //Updating local storage
    updateLocalStorage(trip);
    //Directing user to the welcome page
    window.location ="welcome.html";
  }
}

//Calling function to run as page loads
displaySummary()

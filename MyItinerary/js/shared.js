//This file contains data that runs on all html pages
//It contains class instances used to store trips and routes in a trip list
//Team: Zaeema, Devmal, Shathurshan
//Last Modified: 15/05/2021

"use strict";

const TRIP_DATA_KEY = "tripLocalData";
const TRIP_INDEX_KEY = "selectedTripIndex";
//const ROUTE_DATA_KEY = "routeLocalData";
//const ROUTE_INDEX_KEY = "selectedRouteIndex";

//a class for each individual route
class Route
{
    constructor(origin)
    {
        this._origin = origin; //place of departure
        //this._routeNo = routeNo; // made a route number so each route will have a unique number that can be used when removing or getting a route
        this._destination = ""; //final destination
        this._distance = "";
        this._flightNo = "";
        this._originCoord=[];
        this._destinationCoord=[];
    }

    get origin() { return this._origin; }
    get destination() {return this._destination; }
    get distance() { return this._distance; }
    get flightNo() { return this._flightNo; }
    get originCoord() { return this._originCoord }
    get destinationCoord() { return this._destinationCoord }

    set origin(origin) { this._origin=origin; }
    set destination(destination) { this._destination=destination; }
    set distance(distance) { this._distance=distance; }
    set flightNo(number) { this._flightNo=number; }
    set originCoord(originCoord) { this._originCoord=originCoord; }
    set destinationCoord(destinationCoord) {this._destinationCoord=destinationCoord; }

    removeOriginCoord(index)
    {
        this._originCoord.splice(index,1);
    }

    removeDestinationCoord(index)
    {
        this._destinationCoord.splice(index,1);
    }

    fromData(data)
    {
        this._origin = data._origin;
        this._destination = data._destination;
        this._distance = data._distance;
        this._flightNo = data._flightNo;
        this._originCoord = data._originCoord;
        this._destinationCoord = data._destinationCoord;
    }
}

//a class for each individual trip which will contain an array of route classes
class Trip
{
    constructor(name,country,date,time)
    {
        this._country = country;
        this._date = date;
        this._time = time;
        this._name = name;
        this._routes = [];
        this._source = ""; // source location where the trip starts from 
        this._end = ""; // end location where the trip ends
        this._totaldistance = "" // Shathurshan, you'll need to add up the distances from each route and then update this in trips
    }

    get name() { return this._name; }
    get country() { return this._country;}
    get date() { return this._date;}
    get time() { return this._time;}
    get routes() { return this._routes;}
    get routesCount() { return this._routes.length; }
    get source() { return this._source;}
    get end() { return this._end }
    get totaldistance() { return this._totaldistance }

    set name(name) { this._name=name; }
    set country(country) { this._country = country; }
    set date(date) { this._date = date; }
    set time(time) { this._time = time; }
    set source(source) { this._source = source; }
    set end(end) { this._end = end; }
    set totaldistance(totaldistance) { this._totaldistance = totaldistance; }

    addRoute(origin)
    {
        let newRoute = new Route(origin);
        this._routes.push(newRoute);
    }

    getRoute(index)
    {
        return this._routes[index];
    }

    removeRoute(index)
    {
        this._routes.splice(index,1);
    }

    fromData(data)
    {
        this._country = data._country;
        this._date = data._date;
        this._time = data._time;
        this._name = data._name;
        this._routes = [];
        this._source = data._source;
        this._end = data._end;
        this._totaldistance = data._totaldistance;
        let savedArray=data._routes;
        for (let i=0; i<savedArray.length; i++)
        {
            let newRoute = new Route();
            newRoute.fromData(savedArray[i]);
            this._routes.push(newRoute);
        }
    }
}

//a class to store all trips made, containing an array of Trip instances
class TripList
{
    constructor()
    {
        this._trips = [];
    }

    get trips() { return this._trips; }
    get tripCount() { return this._trips.length; }

    addTrip(name,country,date,time)
    {
        let newTrip = new Trip(name,country,date,time)
        this._trips.push(newTrip);
    }

    getTrip(index)
    {
        return this._trips[index];
    }

    removeTrip(index)
    {
        this._trips.splice(index,1);
    }

    fromData(data)
    {
        this._trips = [];
        let savedArray=data._trips;
        for (let i=0; i<savedArray.length; i++)
        {
            let newTrip = new Trip();
            newTrip.fromData(savedArray[i]);
            this._trips.push(newTrip);
        }
    }
}

// Global TripList instance
let trips = new TripList();

//This function checks if there is data already stored in local storage
//If there is data, then it returns true and if there is no data it returns false
function checkIfDataExists()
{
    let storedTrips = localStorage.getItem("TRIP_DATA_KEY")
    if (storedTrips===null||storedTrips===undefined||storedTrips==="")
    {
        return false;
    }
    else
    {
        return true
    }
}

/*function checkIfRouteDataExists()
{
    let storedRoutes = localStorage.getItem("ROUTE_DATA_KEY")
    if (storedRoutes===null||storedRoutes===undefined||storedRoutes==="")
    {
        return false;
    }
    else
    {
        return true
    }
}*/

//This function updates local storage with new data
//arguments: data: data that needs to be saved to the local storage
function updateLocalStorage(data)
{
    let stringData=JSON.stringify(data);
    localStorage.setItem("TRIP_DATA_KEY",stringData);
}

//This function gets data that is stored in local storage and returns this data 
function getDataLocalStorage()
{
    let objectJSONData = localStorage.getItem("TRIP_DATA_KEY")
    let objectData = JSON.parse(objectJSONData);
    return objectData;
}

//Code that will run on load
//This code runs the checkIfDataExists() function as soon as the page is loaded
//if the data exists it retrieves this data
if (checkIfDataExists()===true)
{
	let newData = getDataLocalStorage();
    trips.fromData(newData);
}

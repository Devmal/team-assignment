/*All planned trips will be visible to viewer. Page creates an html card for each
trip with last trip being the first card on display. html elements are updated with
trip information from local storage, thus giving tripinformation on the card. User can
either delete a trip or go to the detailed trip information page from each individual trip
card*/

"use strict";
//Function to display the card with trip Details
function displayPlannedTrips()
{
  //Accessing the html division
  let outputArea = document.getElementById("tripDisplay");
  //Clearing default trip card
  outputArea.innerHTML="";
  //Setting output as an empty string to be updated with HTML elements
  let output = "";
  //Looping over all Trips
  for(let i=trips.trips.length-1; i>=0; i--)
  {
      //Getting the trip number, name, date, starting point, final destination, stops and total distance
      let tripNumber = i+1;
      let tripName = trips.trips[i].name;
      let tripDate = trips.trips[i].date;
      let tripSource = trips.trips[i].source;
      let tripFinal=""
      if (trips.trips[i].routes.length===0)
      {
        tripFinal = tripSource;
      }
      else
      {
        tripFinal = trips.trips[i].routes[trips.trips[i].routes.length-1]._origin;
      }
      
      let today = new Date();
let date = parseInt(today.getDate())//.padStart(2, '0');
let month = parseInt(today.getMonth() + 1)//.padStart(2, '0');
let year = parseInt(today.getFullYear());

//today = mm + '/' + dd + '/' + yyyy;


//let x = "05/16/2021";

let date_trip = parseInt(tripDate.substr(8));
let month_trip = parseInt(tripDate.substr(6,7));
let year_trip = parseInt(tripDate.substr(0,4));


let tripStatus ="";
if(year_trip===year)
  {
    if(month_trip===month)
      {
        if(date<date_trip)
          {
            tripStatus += "Future";
          }
        else if(date_trip<date)
          {
            tripStatus += "History";
          }
        else if (date_trip===date)
          {
            tripStatus += "Commenced";
          }
      }
      else
      {
        if(month<month_trip)
        {
          tripStatus+="Future";
        }
        else
        {
          tripStatus += "History"
        }
      }
      }
    else
    {
     if(year<year_trip)
     {
      tripStatus += "Future";
     }
     else
     {
      tripStatus += "History"
     }
    }
    
    
      //let tripFinal = trips.trips[trips.trips[i].routes.length-1].routes[trips.trips[i].routes.length-1]._origin;
      let tripStops=""
      if (trips.trips[i].routes.length===1)
      { tripStops=0; }
      else {tripStops = trips.trips[i].routes.length-2;}

      let tripTotDistance=0;
      let tripDistance=""
      for (let i=0;i<trips.trips[trips.trips.length-1].routes.length;i++)
      {
        tripTotDistance += Number(trips.trips[trips.trips.length-1].routes[i].distance)
      }
      tripDistance = tripTotDistance.toFixed(2) + " km"
      //let tripDistance = trips.trips[i].totaldistance;


      //Updating output with HTML elements
      output+="<div class='mdl-cell mdl-cell--3-col'><div class='event_1'><style>.demo-card-event.mdl-card {width: 300px;height: 320px;background: #ebebeb;}"
      output+=".demo-card-event > .mdl-card__actions {border-color: rgba(255, 255, 255, 0.2);}.demo-card-event > .mdl-card__title {align-items: flex-start;}.demo-card-event > .mdl-card__title > h4 {margin-top: 0;}.demo-card-event >"
      output+=".mdl-card__actions {display: flex;box-sizing:border-box;align-items: center;}.demo-card-event > .mdl-card__actions > .material-icons {padding-right: 10px;}.demo-card-event >"
      output+=".mdl-card__title,.demo-card-event > .mdl-card__actions,.demo-card-event > .mdl-card__actions > .mdl-button {color: #fff;}</style>"
      output+="<div class='demo-card-event mdl-card mdl-shadow--2dp'><div class='mdl-card__title mdl-card--expand'>"
      output+="<p style='font-size:16px; color:black;'>"
      output+="Trip: "+tripNumber+"<br>"+tripName+"<br>Date of Departure: "+tripDate+"<br>Starting Point: "+tripSource+"<br>Final Destination: "+tripFinal+"<br>Stops: "+tripStops+"<br>Total Distance: "+tripDistance+"<br>Status: "+tripStatus+"</p></div>"
      output+="<div class='mdl-card__actions mdl-card--border'><a class='mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect mdl-color--teal-500' style='font-size:12px; color:black;' onclick='tripDetails(" + i + ")'><b>Trip Details</b></a>"
      output+="<div class='mdl-layout-spacer'></div><div class='mdl-layout-spacer'></div><a class='mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect mdl-color--teal-500' style='font-size:12px; color:black;' onclick='deleteTrip(" + i + ")'><b>Delete Trip</b></a>"
      output+="</div></div></div></div>"
      output+="<div class='mdl-layout-spacer'></div>"


  }
  //Adding output to HTML page
  outputArea.innerHTML += output;
}


//Function to go to trip Details
function tripDetails(i)
{
  //Getting particular trip
  //details(i)
  let data=i;
  let stringData=JSON.stringify(data);
  localStorage.setItem("selectedTripIndex",stringData);
  //let objectJSONData = localStorage.getItem("selectedTripIndex")
  //let objectData = JSON.parse(objectJSONData);
  window.location="finalPlannedRoute.html";
}



//Function to delete trips
function deleteTrip(i)
{
  if(confirm("Are you sure you want to delete this trip?"))
  //User wishes to do so
  {
    //delete trip using removeTrip method
    trips.removeTrip(i);
    //Updating local storage and informing user
    updateLocalStorage(trips);
    alert("You trip has been deleted");
    displayPlannedTrips() //displaying remaining cards with trips
  }
}
//Code that will run on page
displayPlannedTrips();

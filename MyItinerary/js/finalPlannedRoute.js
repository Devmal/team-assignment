{

  "use strict";  
  getDataLocalStorage()

    let objectJSONData = localStorage.getItem("selectedTripIndex")
    let objectData = JSON.parse(objectJSONData);
    //console.log(objectData)
    //console.log(trips)
    //var finalCoordinates = JSON.parse(localStorage.getItem("routeData"));

    let finalCoordinates=[];
    for (let i=0; i<trips.trips[objectData].routes.length;i++)
    {
      finalCoordinates.push([trips.trips[objectData].routes[i].originCoord]);
    }
    console.log(finalCoordinates)

    //localStorage.removeItem("routeData");

    let date = trips.trips[objectData]._date;
    let routes = trips.trips[objectData]._routes;



    let map = new mapboxgl.Map({
              container: 'map',
              style: 'mapbox://styles/mapbox/streets-v10',
              zoom: 3,
              center: finalCoordinates[0][0]
            });


    var IconData =  []


                   for(i = 0;i < finalCoordinates.length;i++)
                {

                       var data = {
                       'type': 'Feature',
                       'properties': {
                       'icon-color': '#00ffff'
                                },
                       'geometry': {
                       'type': 'Point',
                       'coordinates': finalCoordinates[i][0],
                        }
                    }

                    IconData.push(data)


                        if (i !== finalCoordinates.length -1)
                       {
                         IconData.push(',')
                       }
                 }


                 var geojson = {
                     'type': 'FeatureCollection',
                     'features':[IconData]
                 }


                 geojson.features[0].forEach(function (marker) {


                         if (marker !== ",")
                        {
                            // Create a DOM element for each marker.
                         var el = document.createElement('div');
                         el.className = 'marker';
                         el.style.backgroundImage = 'url(https://cdn.rawgit.com/mapbox/mapbox-gl-styles/master/sprites/bright-v9/_svg/airfield-15.svg)';
                         el.style.width = '20px';
                         el.style.height = '20px';
                         el.style.backgroundColor = '#00ffff'
                         el.style.cursor = "pointer"

                         // Add markers to the map.
                         new mapboxgl.Marker(el)
                             .setLngLat(marker.geometry.coordinates)
                             .addTo(map);


                        }


                   });


                 var polylineData = [];

                 // Function to display the routes
                 function displayRoutes()
                {

                       for (var i = 0;i<finalCoordinates.length-1;i++)
                      {

                         var data =
                        {
                          'type': 'Feature',
                          'geometry': {
                          'type': 'LineString',
                          'coordinates': [finalCoordinates[i][0],finalCoordinates[i+1][0]]
                               }
                        }


                        polylineData.push(data)

                        if (i !== polylineData.length -1)
                       {
                         polylineData.push(',')
                       }

                     }




                    map.on('load', function () {
                    map.addSource('multiple-lines-source', {
                                  'type': 'geojson',
                                  'data': {
                                  'type': 'FeatureCollection',
                                  'features': polylineData
                                        },
                                  });

                    map.addLayer({
                                 'id': 'multiple-lines-layer',
                                 'type': 'line',
                                 'source': 'multiple-lines-source',
                                 'layout': {},
                                 'paint': {
                                           'line-color': '#000',
                                           'line-width': 2.5
                                          },
                                  });
                          });



              }

               displayRoutes()
    let outputArea = document.getElementById("info");//Change // ID

    //  let travelRoute="";
      let output = "";
      for(let i = 0; i < routes.length-1; i++)
      {
        let origin = routes[i]._origin;
        let destination = routes[i]._destination;
        //let travelRoute += origin + "-" destination;
        output += '<li class="mdl-list__item mdl-list__item--two-line" >' +
            '<span class="mdl-list__item-primary-content">' +
            '<span>' + "Arrival :" + origin + '</span>' + "</br>" +
            '<span>' + "Destination :" + destination + '</span>' + "</br>" +
              '<span>' + "Date :" + date + '</span>' + "</br>" +
            '</span>' +
            '</li>'; +
            "<div class='mdl-layout-spacer'></div>"
      }

      outputArea.innerHTML = output;





}

function save()
{
  //localStorage.removeItem("routeData");
  window.location = "welcome.html";
}